val akkaVersion      = "2.5.31"
val auvioVersion     = "1.0.3.jre11"
val codecVersion     = "1.14"
val configVersion    = "1.4.0"
val jansiVersion     = "1.18"
val jettyVersion     = "9.4.28.v20200408"
val json4sVersion    = "3.6.7"
val mbarix4jVersion  = "2.0.5.jre11"
val scalatestVersion = "3.0.8"
val scalatraVersion  = "2.7.0"
val scilubeVersion   = "2.0.7.jre11"
val servletVersion   = "4.0.1"
val slf4jVersion     = "2.0.0-alpha1"
val logbackVersion   = "1.3.0-alpha4"

Global / onChangedBuildSource := ReloadOnSourceChanges

lazy val buildSettings = Seq(
  organization := "org.mbari.auv",
  version := "0.0.3",
  scalaVersion in ThisBuild := "2.13.1",
  crossScalaVersions := Seq("2.13.1"),
  organizationName := "Monterey Bay Aquarium Research Institute",
  startYear := Some(2017),
  licenses += ("Apache-2.0", new URL(
    "https://www.apache.org/licenses/LICENSE-2.0.txt"
  ))
)

lazy val consoleSettings = Seq(
  shellPrompt := { state =>
    val user = System.getProperty("user.name")
    user + "@" + Project.extract(state).currentRef.project + ":sbt> "
  },
  initialCommands in console :=
    """
      |import java.time.Instant
      |import java.util.UUID
    """.stripMargin
)

lazy val dependencySettings = Seq(
  resolvers ++= Seq(
    Resolver.mavenLocal,
    "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/",
    "Sonatype OSS Releases" at "https://oss.sonatype.org/content/repositories/releases/",
    "hohonuuli-bintray" at "https://dl.bintray.com/hohonuuli/maven",
    "mbari-bintray" at "https://dl.bintray.com/org-mbari/maven",
    "unidata repo" at "https://artifacts.unidata.ucar.edu/content/repositories/unidata-releases/",
    "geotoolkit repo" at "https://maven.geotoolkit.org"
  )
)

lazy val optionSettings = Seq(
  scalacOptions ++= Seq(
    "-deprecation",
    "-encoding",
    "UTF-8", // yes, this is 2 args
    "-feature",
    "-language:existentials",
    "-language:implicitConversions",
    "-unchecked",
    "-Xlint"
  ),
  javacOptions ++= Seq("-target", "11", "-source", "11"),
  updateOptions := updateOptions.value.withCachedResolution(true)
)

lazy val appSettings = buildSettings ++
  consoleSettings ++
  dependencySettings ++
  optionSettings ++ Seq(
  fork := true
)

val apps = Map("jetty-main" -> "JettyMain", "generate-cache-app" -> "GenerateCacheApp") // for sbt-pack

lazy val `auv-rawdata-server` = (project in file("."))
  .enablePlugins(JettyPlugin)
  .enablePlugins(AutomateHeaderPlugin)
  .enablePlugins(PackPlugin)
  .settings(appSettings)
  .settings(
    libraryDependencies ++= Seq(
      "ch.qos.logback"       % "logback-classic"   % logbackVersion % "runtime",
      "com.typesafe"         % "config"            % configVersion,
      "com.typesafe.akka"    %% "akka-actor"       % akkaVersion,
      "commons-codec"        % "commons-codec"     % codecVersion,
      "javax.servlet"        % "javax.servlet-api" % servletVersion,
      "org.eclipse.jetty"    % "jetty-server"      % jettyVersion % "container;compile;test",
      "org.eclipse.jetty"    % "jetty-servlets"    % jettyVersion % "container;compile;test",
      "org.eclipse.jetty"    % "jetty-webapp"      % jettyVersion % "container;compile;test",
      "org.fusesource.jansi" % "jansi"             % jansiVersion % "runtime",
      "org.json4s"           %% "json4s-native"    % json4sVersion,
      "org.mbari"            % "mbarix4j"          % mbarix4jVersion % "test",
      "org.mbari.auv"        %% "auv-log-io"       % auvioVersion,
      "org.scalatest"        %% "scalatest"        % scalatestVersion % "test",
      "org.scalatra"         %% "scalatra"         % scalatraVersion,
      "org.scalatra"         %% "scalatra-json"    % scalatraVersion,
      "org.scalatra"         %% "scalatra-scalate" % scalatraVersion,
      "org.scalatra"         %% "scalatra-swagger" % scalatraVersion,
      "org.slf4j"            % "log4j-over-slf4j"  % slf4jVersion % "runtime",
      "org.slf4j"            % "slf4j-api"         % slf4jVersion
    ).map(
      _.excludeAll(
        ExclusionRule("org.slf4j", "slf4j-jdk14"),
        ExclusionRule("javax.servlet", "servlet-api")
      )
    )
  )
  .settings( // config sbt-pack
    packMain := apps,
    packExtraClasspath := apps
      .keys
      .map(k => k -> Seq("${PROG_HOME}/conf"))
      .toMap,
    packJvmOpts := apps
      .keys
      .map(k => k -> Seq("-Duser.timezone=UTC", "-Xmx4g", "-Djdk.nio.maxCachedBufferSize=262144"))
      .toMap,
    packDuplicateJarStrategy := "latest",
    packJarNameConvention := "original"
  )
// https://www.evanjones.ca/java-bytebuffer-leak.html

// -- Aliases ---------------------------------------------------------
addCommandAlias("cleanall", ";clean;clean-files")
