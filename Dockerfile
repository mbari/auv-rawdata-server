FROM openjdk:11

MAINTAINER Brian Schlining <bschlining@gmail.com>

ENV APP_HOME /opt/auv-rawdata-server

RUN mkdir ${APP_HOME}

COPY target/pack ${APP_HOME}

RUN groupadd -r --gid 716 docker && \
    useradd -r --uid 11472 -g docker docker_user && \
    chgrp -R docker ${APP_HOME} && \
    chmod -R 755 ${APP_HOME} && \
    chmod a+rx ${APP_HOME}/bin/jetty-main

USER docker_user

EXPOSE 8080

ENTRYPOINT ${APP_HOME}/bin/jetty-main