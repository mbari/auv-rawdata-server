# How to deploy

To deploy do the following:

## On Development computer

Build and deploy a new docker image

```
sbt pack
docker build -t hohonuuli/auv-rawdata-server .
docker login --username=hohonuuli
docker push hohonuuli/auv-rawdata-server
```

## On Portal

```
docker pull hohonuuli/auv-rawdata-server
sudo /usr/bin/systemctl restart docker.auv-rawdata-server
```

# Setup for deployment

This has already been done, but these notes are for reference.

1. Copy `docker.auv-rawdata-server.service` onto portal at `/etc/systemd/system`. I did it as myself (brian) and did not have to monkey with file permissions at all.
2. Run a test using `/usr/bin/systemctl start docker.auv-rawdata-server` and verify that it works.
3. Enable it with `/usr/bin/systemctl enable docker.auv-rawdata-server`. You can verify the status using `/usr/bin/systemctl status docker.auv-rawdata-server`