/*
 * Copyright 2017 Monterey Bay Aquarium Research Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.mbari.auv.nio;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * @author Brian Schlining
 * @since 2017-03-27T16:38:00
 */
public class Directories {

    private static final Logger log = LoggerFactory.getLogger(Directories.class);

    public static List<Path> list(Path path, Predicate<Path> predicate) {

        List<Path> children = new ArrayList<>();
        try (DirectoryStream<Path> ds = Files.newDirectoryStream(path, predicate::test)) {
            for (Path child: ds) {
                children.add(child);
            }
        }
        catch (IOException e) {
            log.warn("Error listing files in " + path.toAbsolutePath().toString());
        }

        return children;
    }

    public static List<Path> list(Path path) {
        List<Path> children = new ArrayList<>();
        try (DirectoryStream<Path> ds = Files.newDirectoryStream(path)) {
            for (Path child: ds) {
                children.add(child);
            }
        }
        catch (IOException e) {
            log.warn("Error listing files in " + path.toAbsolutePath().toString());
        }

        return children;
    }

    public static List<Path> list(Path path, String glob) {
        List<Path> children = new ArrayList<>();
        try (DirectoryStream<Path> ds = Files.newDirectoryStream(path, glob)) {
            for (Path child: ds) {
                children.add(child);
            }
        }
        catch (IOException e) {
            log.warn("Error listing files in " + path.toAbsolutePath().toString());
        }

        return children;
    }
}
