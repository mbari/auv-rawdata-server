/*
 * Copyright 2017 Monterey Bay Aquarium Research Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import akka.actor.ActorSystem

import javax.servlet.ServletContext
import org.mbari.auv.rawdata.api.DefaultV1Api
import org.mbari.auv.rawdata.controllers.InMemDataController
import org.mbari.auv.rawdata.db.CacheFile
import org.scalatra.LifeCycle
import org.mbari.auv.rawdata.{AppConfig, ResourcesApp, SwaggerApp}
import org.mbari.auv.rawdata.model.{CachedMissionDir, Mission}
import org.slf4j.LoggerFactory

class ScalatraBootstrap extends LifeCycle {

  private[this] val log = LoggerFactory.getLogger(getClass)

  implicit val swagger = new SwaggerApp

  override def init(context: ServletContext): Unit = {
    implicit val system: ActorSystem = ActorSystem("appActorSystem")
    try {

      // -- Read AUV directories from config file
      val auvDirectories = AppConfig.auvDirectories

      // -- Read cache file location from config file
      val cacheFile = {
        val file = AppConfig.dataCacheFile.toFile
        if (file.exists() && file.canRead) {
          log.info(s"Found data cache at ${file.getAbsolutePath}")
          Option(file)
        }
        else {
          log.info(s"No data cache at ${file.getAbsolutePath}")
          None
        }
      }

      // -- Import cache file if it exists
      val cachedDeployments = cacheFile
        .map(CacheFile(_))
        .getOrElse(Nil)
        .map(CachedMissionDir)
        .map(_.asInstanceOf[Mission])
        .toArray

      // -- Create our data controller.
      val dataController = new InMemDataController(auvDirectories, cachedDeployments)

      // -- Configure API
      val v1Api = new DefaultV1Api(dataController)

      context mount (v1Api, "/v1", "v1")
      context mount (new ResourcesApp, "/api-docs")
    }
    catch {
      case e: Throwable => e.printStackTrace()
    }
  }
}
