/*
 * Copyright 2017 Monterey Bay Aquarium Research Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.mbari.auv.rawdata.db

import java.nio.file.{Files, Path}

import org.mbari.auv.nio.Directories
import org.mbari.auv.rawdata.AppConfig
import org.mbari.auv.rawdata.model.{Mission, MissionDir}
import org.slf4j.LoggerFactory

import scala.util.Try
import scala.collection.JavaConverters._
import scala.collection.mutable

/**
  *
  *
  * @author Brian Schlining
  * @since 2016-02-24T19:07:00
  */
class InMemoryDatastore(
    val roots: Iterable[Path],
    val missions: Set[Mission]
)

object InMemoryDatastore {

  private[this] val log       = LoggerFactory.getLogger(getClass)
  private[this] val cachePath = AppConfig.dataCacheFile

  class LoadParams(val remainingDirs: Iterable[Path], loadedMissions: Iterable[Mission]) {
    val missions: Array[Mission] = loadedMissions.toArray.sortBy(_.absolutePath)
  }

  /**
    *
    * @param dirs
    * @param cachedMissions
    * @return
    */
  def apply(dirs: Iterable[Path], cachedMissions: Iterable[Mission]): InMemoryDatastore = {

    val loadParams0 = new LoadParams(dirs, cachedMissions)

    val loadParams1 = loadCatalogedMissions(loadParams0)
    val missions    = loadRemainingMissions(loadParams1) ++ loadParams1.missions

    // -- Update cache
    CacheFile.writeAsync(missions.map(_.asDeployment), cachePath)

    // -- Voila
    new InMemoryDatastore(dirs, missions.toSet)
  }

  def loadCatalogedMissions(loadParams: LoadParams): LoadParams = {
    val catalogName   = AppConfig.dataCatalogFile
    val remainingDirs = new mutable.ArrayBuffer[Path]
    val catalogedMissions = for (dir <- loadParams.remainingDirs) yield {
      val catalogPath = dir.resolve(catalogName)
      if (Files.exists(catalogPath)) {
        CatalogFile(catalogPath)
      }
      else {
        remainingDirs += dir
        Nil
      }
    }

    val loadedMissions = catalogedMissions
      .flatten
      .toSet
      .union(loadParams.missions.toSet)

    new LoadParams(remainingDirs, loadedMissions)
  }

  def loadRemainingMissions(loadParams: LoadParams): Array[Mission] =
    loadParams
      .remainingDirs
      .flatMap(dir => findMissions(dir, loadParams.missions))
      .toArray
      .sortBy(_.absolutePath)

  def findMissions(
      path: Path,
      cachedMissions: Array[Mission],
      accum: Set[Mission] = Set.empty
  ): Set[Mission] = {
    if (isWalkable(path)) {
      //val idx = Mission.binarySearch(cachedMissions, path.toAbsolutePath.toString)
      //log.debug("IDX: {} for {}", idx, path.toAbsolutePath.toString)
      val existing = cachedMissions.find(p => p.absolutePath.equals(path.toAbsolutePath.toString))
      existing match {
        case None =>
          log.debug(s"Examining $path")
          val childFiles = Directories.list(path).asScala
          if (isAuvMission(childFiles)) accum + new MissionDir(path)
          else
            Try(childFiles.flatMap(d => findMissions(d, cachedMissions, accum)))
              .map(_.toSet)
              .toOption
              .getOrElse(accum)
        case Some(m) => accum + m
      }

      //      if (idx >= 0) {
      //        log.debug("Using cached data for {}", cachedMissions(idx).absolutePath)
      //        accum + cachedMissions(idx)
      //      } else {
      //        log.debug(s"Examining $path")
      //        val childFiles = Directories.list(path).asScala
      //        if (isAuvMission(childFiles)) accum + new MissionDir(path)
      //        else Try(childFiles.flatMap(d => findMissions(d, cachedMissions, accum)))
      //          .map(_.toSet)
      //          .toOption
      //          .getOrElse(accum)
      //      }
    }
    else accum
  }

  /**
    * Checks that the contents of a directory contains an AUV mission. This
    * is done by the presence of both the navigation.log and vehicle.cfg files.
    *
    * @param paths
    * @return
    */
  def isAuvMission(paths: Iterable[Path]): Boolean = {
    val filesnames = paths.map(_.getFileName.toString)
    val navlog     = filesnames.find(_.equalsIgnoreCase("navigation.log"))
    val cfg        = filesnames.find(_.equalsIgnoreCase("vehicle.cfg"))
    navlog.isDefined && cfg.isDefined
  }

  /**
    * Note that this does not follow symbolic links and ignores directories that end with '.dir'
    * as those are Dave Caress' special directories that may contain millions of files and really,
    * really slow down the update process
    * @param path
    * @return
    */
  private def isWalkable(path: Path): Boolean =
    Files.isDirectory(path) &&
      !Files.isSymbolicLink(path) &&
      !path
        .toAbsolutePath
        .toString
        .contains(".snapshot") &&                 // Ignore .snapshot directories on Atlas
      !path.getFileName.toString.endsWith(".dir") // These contains lots of files
  /**
    * Rescans the mission directories in the provided data store and returns a new InMemoryDatastore instance
    *
    * @param datastore
    * @return
    */
  def update(datastore: InMemoryDatastore): InMemoryDatastore =
    apply(datastore.roots, datastore.missions)

}
