/*
 * Copyright 2017 Monterey Bay Aquarium Research Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.mbari.auv.rawdata.db

import java.io.File
import java.net.URL
import java.nio.charset.Charset
import java.nio.file.{Files, Path, Paths}
import java.time.Instant

import org.mbari.auv.rawdata.model.Deployment
import org.json4s._
import org.json4s.native.JsonMethods._
import org.json4s.native.Serialization
import org.json4s.native.Serialization.writePretty
import org.slf4j.LoggerFactory

import scala.io.Source
import scala.util.control.NonFatal

/**
  * Reads and writes the cached mission data
  *
  * @author Brian Schlining
  * @since 2016-03-08T10:33:00
  */
object CacheFile {

  private[this] val log = LoggerFactory.getLogger(getClass)

  // JSON4S date serializer was broken and would not do a round trip
  // override it and use our own formater
  //  private val dateFormat = {
  //    val df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
  //    df.setTimeZone(TimeZone.getTimeZone("UTC"))
  //    df
  //  }
  //
  //  private[this] val customDateFormat = new CustomSerializer[Date](format => (
  //    { case JString(s) => dateFormat.parse(s) },
  //    { case d: Date => JString(dateFormat.format(d)) }
  //  ))

  private[this] val customInstantFormat = new CustomSerializer[Instant](format =>
    (
      { case JString(s) => Instant.parse(s) },
      { case i: Instant => JString(i.toString) }
    )
  )

  private[this] val pathFormat = new CustomSerializer[Path](format =>
    (
      { case JString(s) => Paths.get(s) },
      { case p: Path    => JString(p.toAbsolutePath.toString) }
    )
  )

  /**
    * Read the cached mission data from a file
    * @param cacheFile The JSON file to read
    * @return A list of Deployments that are listed in the file
    */
  def apply(cacheFile: File): Iterable[Deployment] = {
    val source      = Source.fromFile(cacheFile)
    val text        = source.mkString
    val deployments = jsonToDeployments(text)
    source.close()
    deployments
  }

  /**
    * Read cached missions from a remote URL
    * @param cacheURL
    * @return
    */
  def apply(cacheURL: URL): Iterable[Deployment] = {
    val source      = Source.fromURL(cacheURL)
    val text        = source.mkString
    val deployments = jsonToDeployments(text)
    source.close()
    deployments
  }

  def jsonToDeployments(s: String): Iterable[Deployment] = {
    implicit val formats = DefaultFormats + customInstantFormat + pathFormat
    parse(s).extract[Array[Deployment]]
  }

  def deploymentsToJson(deployments: Iterable[Deployment]): String = {
    implicit val formats = Serialization.formats(NoTypeHints) + customInstantFormat + pathFormat
    val sortedDeployment = deployments.toArray.sortBy(d => d.path.toAbsolutePath.toString)
    writePretty(sortedDeployment)
  }

  def writeAsync(deployments: Iterable[Deployment], target: Path): Unit = {
    val thread = new Thread(() => {
      log.info("Writing deployment cache to {}", target.toAbsolutePath)
      try {
        val cacheData = CacheFile.deploymentsToJson(deployments)
        val writer    = Files.newBufferedWriter(target, Charset.forName("UTF-8"))
        writer.write(cacheData)
        writer.close()
      }
      catch {
        case NonFatal(e) => log.warn(s"Failed to write ${target.toAbsolutePath}", e)
      }
    }, "CacheFile-writeAsync")
    thread.setDaemon(true)
    thread.start()
  }

}
