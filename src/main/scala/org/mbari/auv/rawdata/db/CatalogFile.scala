/*
 * Copyright 2017 Monterey Bay Aquarium Research Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.mbari.auv.rawdata.db

import java.nio.file._

import org.mbari.auv.rawdata.model.{Mission, MissionDir}
import org.slf4j.LoggerFactory

import scala.io.Source

/**
  * Parse catalog files. These files should be located at the root of any directory listed
  * in the config file at `auv.directories`. They contains a listing of all the valid AUV missions
  * in that directory. If a catalog files is found, then that directory will not be walked. Only
  * the missions in the files will be used.
  * @author Brian Schlining
  * @since 2017-03-29T08:31:00
  */
object CatalogFile {

  private[this] val log = LoggerFactory.getLogger(getClass)

  def apply(
      path: Path
  ): Iterable[Mission] = {
    log.debug("Loading catalog from {}", path.toAbsolutePath.toString)
    val source = Source.fromFile(path.toFile)
    val lines = source
      .getLines()
      .map(_.trim)
      .filter(_.nonEmpty)
      .filter(!_.startsWith("#"))
    val parent = Option(path.getParent).getOrElse(Paths.get("/"))
    val missions = lines
      .map(l => parent.resolve(l))
      .filter(p => Files.exists(p))
      .map(p => new MissionDir(p))
      .toSet
    source.close()
    missions
  }
}
