/*
 * Copyright 2017 Monterey Bay Aquarium Research Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.mbari.auv.rawdata.controllers

import java.nio.file.Path
import java.time.Instant

import org.mbari.auv.io.{LogRecord, LogRecordDescription, LogRecordReader}
import org.mbari.auv.nio.Directories
import org.mbari.auv.rawdata.db.InMemoryDatastore
import org.mbari.auv.rawdata.model.Mission
import org.slf4j.LoggerFactory

import scala.jdk.CollectionConverters._
import scala.util.Try

/**
  * Reads the AUV paths from the environment variable AUV_PATHS and generates an
  * in memory data model for them
  *
  * @author Brian Schlining
  * @since 2016-02-25T15:39:00
  */
class InMemDataController(
    val auvDirectories: Iterable[Path],
    cachedMissions: Array[Mission] = Array.empty[Mission]
) extends DataController {

  private[this] val log = LoggerFactory.getLogger(getClass)

  if (log.isDebugEnabled) {
    log.debug(
      "Using the following AUV roots: " +
        auvDirectories.map(_.toAbsolutePath.toString).mkString(":")
    )
  }

  @volatile
  private[this] var datastore: InMemoryDatastore = InMemoryDatastore(auvDirectories, cachedMissions)

  def deployment(name: String): Option[Mission] = {
    val matches = datastore
      .missions
      .filter(_.name.equalsIgnoreCase(name))
    if (matches.isEmpty) None else Option(matches.head)
  }

  def missions: Set[Mission] = datastore.missions

  def missions(from: Option[Instant] = None, to: Option[Instant] = None): Set[Mission] = {
    val xs = datastore.missions
    from match {
      case None =>
        to match {
          case None     => xs
          case Some(t1) => xs.filter(_.overlaps(Instant.MIN, t1))
        }
      case Some(t0) =>
        to match {
          case None     => xs.filter(_.overlaps(t0, Instant.MAX))
          case Some(t1) => xs.filter(_.overlaps(t0, t1))
        }
    }
  }

  def files(deploymentName: String): Seq[Path] = {
    val mission = deployment(deploymentName)
    mission match {
      case None     => Nil
      case Some(md) => Directories.list(md.path).asScala.toSeq
    }
  }

  def data(log: Path): Seq[LogRecord[Double]] = Try(LogRecordReader.read(log.toFile)).getOrElse(Nil)

  def header(log: Path): Seq[LogRecordDescription] =
    Try(LogRecordReader.readHeader(log.toFile)).getOrElse(Nil)

  /**
    * Tells the Data controller to rescan the mounted directories for new mission directories. This
    * will NOT pick up changes to existing mission directories.
    */
  def updateAsync(): Unit = {
    val thread = new Thread(() => {
      val ds = InMemoryDatastore.update(datastore)
      datastore = ds
    })
    thread.setDaemon(true)
    thread.start()
  }

}
