/*
 * Copyright 2017 Monterey Bay Aquarium Research Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.mbari.auv.rawdata

import java.nio.charset.Charset
import java.nio.file.{Files, Paths}

import org.mbari.auv.rawdata.controllers.InMemDataController
import org.mbari.auv.rawdata.db.CacheFile

import scala.util.control.NonFatal

/**
  *
  *
  * @author Brian Schlining
  * @since 2016-03-07T16:19:00
  */
object GenerateCacheApp {

  def main(args: Array[String]): Unit = {
    try {

      val auvDirectories = AppConfig.auvDirectories

      val dataController = new InMemDataController(auvDirectories)
      val str            = generateCacheData(dataController)

      val target =
        if (args.size > 0) Paths.get(args(0))
        else AppConfig.dataCacheFile

      val writer = Files.newBufferedWriter(target, Charset.forName("UTF-8"))
      writer.write(str, 0, str.size)
      writer.close()
    }
    catch {
      case NonFatal(e) => e.printStackTrace()
    }
  }

  def generateCacheData(dataController: InMemDataController): String = {

    // --- Load start and end dates for all missionDirectories
    val deployments = dataController.missions
    deployments.foreach(_.startDate) // This forces the navigation files to be read
    val info = deployments.map(_.asDeployment)
    CacheFile.deploymentsToJson(info)

  }

}
