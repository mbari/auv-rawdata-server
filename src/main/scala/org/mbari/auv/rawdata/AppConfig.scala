/*
 * Copyright 2017 Monterey Bay Aquarium Research Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.mbari.auv.rawdata

/*
 * Copyright 2017 Monterey Bay Aquarium Research Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.nio.file.{Path, Paths}

import com.typesafe.config.{Config, ConfigFactory}

import scala.util.Try

/**
  * Http Parameters
  * @param port
  * @param stopTimeout
  * @param connectorIdleTimeout
  * @param webapp
  * @param contextPath
  */
case class HttpConfig(
    port: Int,
    stopTimeout: Int,
    connectorIdleTimeout: Int,
    webapp: String,
    contextPath: String
)

/**
  * A simple wrapper around the config object so that we avoid embedding strings like "http.port"
  * all over the code base.
  */
object AppConfig {

  /**
    * The raw underlying configuration loaded from `application.conf`
    */
  val config: Config = ConfigFactory.load();

  /**
    * The HTTP configuration as a case class
    */
  val httpConfig: HttpConfig = {
    val c           = config.getConfig("http")
    val port        = Try(c.getInt("port")).getOrElse(8080)
    val stopTimeout = Try(c.getInt("stop.timeout")).getOrElse(5000)
    val idleTimeout = Try(c.getInt("connector.idle.timeout")).getOrElse(90000)
    val webapp      = Try(c.getString("webapp")).getOrElse("webapp")
    val contextPath = Try(c.getString("context.path")).getOrElse("/")
    HttpConfig(port, stopTimeout, idleTimeout, webapp, contextPath)
  }

  /**
    * This is the location of the file used as a persistent store of missions between
    * server restarts. In a docker container this should be written to a location outside of th
    * container so it persists between restarts
    * @return The cache file location
    */
  val dataCacheFile: Path = Paths.get(config.getString("data.cache.file"))

  val dataCatalogFile: String = Try(config.getString("auv.catalog.file"))
    .getOrElse("AUV_MISSION_CATALOG.txt")

  /**
    * @return A list of paths whose subdirectories can be walked in order to find AUV missions
    */
  val auvDirectories: Seq[Path] =
    config
      .getString("auv.directories")
      .split(":")
      .toIndexedSeq
      .map(Paths.get(_))
}
