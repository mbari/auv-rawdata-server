/*
 * Copyright 2017 Monterey Bay Aquarium Research Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.mbari.auv.rawdata.model

import java.nio.file.Path
import java.time.Instant

case class Deployment(
    path: Path,
    vehicle: String,
    startDate: Instant,
    endDate: Instant
) {
  def name: String                   = path.getFileName.toString
  def asWebDeployment: WebDeployment = WebDeployment(name, vehicle, startDate, endDate)
}

case class WebDeployment(name: String, vehicle: String, startDate: Instant, endDate: Instant)
