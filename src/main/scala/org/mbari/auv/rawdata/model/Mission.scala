/*
 * Copyright 2017 Monterey Bay Aquarium Research Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.mbari.auv.rawdata.model

import java.io.File
import java.nio.file.{Files, Path, Paths}
import java.time.Instant
import java.util.{Comparator, Date, Arrays => JArrays}

import org.mbari.auv.io.{LogRecordReader, MissionReader, VehicleCfgReader}
import org.mbari.auv.io.MissionReader.MissionHeaders
import org.mbari.auv.io.dsl._
import org.mbari.auv.nio.Directories
import org.slf4j.LoggerFactory

import scala.collection.JavaConverters._
import scala.util.Try
import scala.util.control.NonFatal

trait Mission {
  def path: Path
  def logHeaders: MissionHeaders
  def startDate: Option[Instant]
  def endDate: Option[Instant]
  def asDeployment: Deployment
  def name: String
  def vehicle: String

  def absolutePath: String = path.toAbsolutePath.toString

  def canEqual(other: Any): Boolean = other.isInstanceOf[Mission]

  override def equals(other: Any): Boolean = other match {
    case that: Mission =>
      (that canEqual this) &&
        path == that.path
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(path)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }

  /**
    * Checks to see if this mission overlaps with the period bounded by the 2 dates
    * that you provide
    *
    * @param t0 The starting date-time
    * @param t1 The ending date-time
    * @return true if this mission overlaps with those dates, false otherwise
    */
  def overlaps(t0: Instant, t1: Instant): Boolean = {
    require(t0.isBefore(t1), s"Whoops! You have time going backwards: t0: $t0 > t1: $t1")
    if (startDate.isEmpty || endDate.isEmpty) false
    else {
      val m0 = startDate.get
      val m1 = endDate.get

      (t0 == m0) ||
      (t1 == m1) ||
      (m0.isAfter(t0) && m0.isBefore(t1)) ||
      (m1.isAfter(t0) && m1.isBefore(t1))
    }
  }

}

object Mission {

  val KEY_TIME = "time"

  val INSTRUMENT_NAVIGATION = "dataNav"

  def dateToInstant(date: Date): Option[Instant] = {
    if (date.getTime <= 0L) None
    else Option(date.toInstant)
  }

  def instantToDate(instant: Option[Instant]): Date = instant match {
    case None    => new Date(0L)
    case Some(i) => Date.from(i)
  }

  def optionToInstant(instant: Option[Instant]): Instant = instant match {
    case None    => Instant.EPOCH
    case Some(i) => i
  }

  /**
    * Search missions for a match using the absolute path of the mission
    * @param missions The sorted array of missions
    * @param absolutePath The path of the mission we are searching for
    * @return
    */
  def binarySearch(missions: Array[Mission], absolutePath: String): Int = {
    val paths = missions.map(_.absolutePath)
    JArrays.binarySearch(paths, absolutePath, Comparator.naturalOrder[String])
  }
}

/**
  * Represents the data needed to describe an AUV mission (i.e. A directory containing
  * AUV log files from a single deployment). Information is lazily loaded, there's some overhead
  * as the navigation.log is read to extract the start and end times of the mission.
  *
  * @author Brian Schlining
  * @since 2016-02-25T09:05:00
  */
class MissionDir(val path: Path) extends Mission with Extractor {

  protected[this] val log = LoggerFactory.getLogger(getClass)

  lazy val logHeaders: MissionHeaders = MissionReader.readHeaders(path.toFile)

  val name = path.getFileName.toString

  /**
    * The start and end times for a mission are considered to be the start and
    * end times of the navigation.log file. This method is used to locate the
    * navigation.log
    */
  lazy val navigationLog: Option[File] = {
    val nav = logHeaders.filter({
      case (f, rs) =>
        rs.exists(r => r.instrumentName.equalsIgnoreCase(Mission.INSTRUMENT_NAVIGATION))
    })
    if (log.isDebugEnabled) {
      val s = if (nav.isEmpty) "No " else ""
      log.debug(s"${s}navigation.log was found in $path")
    }
    if (nav.isEmpty) None else Option(nav.head._1)
  }

  lazy val vehicle: String = {
    val noname = "undefined"
    val cfg = Try(
      Directories
        .list(path, "vehicle.cfg")
        .asScala
        .headOption
    ).getOrElse(None)

    // function to extract vehicle name
    def parse(path: Path): String = {
      try {
        val map = VehicleCfgReader.parse(path.toFile).toMap
        map("vehicleName")
      }
      catch {
        case NonFatal(e) => noname
      }
    }

    cfg.map(parse).getOrElse(noname)
  }

  /**
    *
    * @return Essential information that is used to locate and filter logs.
    */
  def asDeployment: Deployment = Deployment(
    path,
    vehicle,
    Mission.optionToInstant(startDate),
    Mission.optionToInstant(endDate)
  )

  /**
    * The start and end instants of the mission as defined by the navigation.log
    */
  override lazy val (startDate, endDate): (Option[Instant], Option[Instant]) = navigationLog match {
    case None => (None, None)
    case Some(navLog) =>
      val time = extract(LogRecordReader.read(navLog), Mission.KEY_TIME)
      time match {
        case None => (None, None)
        case Some(t) =>
          val startDate = Instant.ofEpochMilli((t.min * 1000).toLong)
          val endDate   = Instant.ofEpochMilli((t.max * 1000).toLong)
          (Option(startDate), Option(endDate))
      }
  }
}

case class CachedMissionDir(deployment: Deployment) extends Mission {
  override lazy val path: Path = deployment.path

  override lazy val logHeaders: MissionHeaders = MissionReader.readHeaders(path.toFile)

  override def startDate: Option[Instant] = Option(deployment.startDate)

  override def endDate: Option[Instant] = Option(deployment.endDate)

  override lazy val asDeployment: Deployment = deployment

  override lazy val name: String = deployment.path.getFileName.toString

  override def vehicle: String = deployment.vehicle
}
