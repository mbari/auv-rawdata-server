/*
 * Copyright 2017 Monterey Bay Aquarium Research Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.mbari.auv.rawdata

/*
 * Copyright 2017 Monterey Bay Aquarium Research Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import akka.actor.ActorSystem
import org.scalatra.ScalatraServlet
import org.scalatra.swagger._

class ResourcesApp(implicit protected val system: ActorSystem, val swagger: SwaggerApp)
    extends ScalatraServlet
    with NativeSwaggerBase {

  before() {
    response.headers.set("Access-Control-Allow-Origin", "*")
  }

  protected def buildFullUrl(path: String) =
    if (path.startsWith("http")) path
    else {
      val port = request.getServerPort
      val h    = request.getServerName
      val prot = if (port == 443) "https" else "http"
      val (proto, host) =
        if (port != 80 && port != 443) ("http", h + ":" + port.toString) else (prot, h)
      "%s://%s%s%s".format(
        proto,
        host,
        request.getContextPath,
        path
      )
    }
}

class SwaggerApp
    extends Swagger(apiInfo = ApiSwagger.apiInfo, apiVersion = "1.0", swaggerVersion = "1.2")

object ApiSwagger {
  val apiInfo = ApiInfo(
    """auv-rawdata""",
    """A server for accessing RAW AUV data logs""",
    """http://www.mbari.org""",
    ContactInfo("Brian Schlining", "http://www.mbari.org", """brian@mbari.org"""),
    LicenseInfo("""MIT""", """http://opensource.org/licenses/MIT""")
  )
}
