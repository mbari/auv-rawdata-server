/*
 * Copyright 2017 Monterey Bay Aquarium Research Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.mbari.auv.rawdata.api

import java.io.File
import java.time.format.DateTimeFormatter
import java.time.Instant
import org.json4s._
import org.mbari.auv.rawdata.controllers.{DataController, InMemDataController}
import org.mbari.auv.rawdata.model.{Deployment, MissionFiles}
import org.scalatra._
import org.scalatra.json.NativeJsonSupport
import org.scalatra.swagger._
import org.scalatra.util.conversion.TypeConverter
import org.slf4j.LoggerFactory
import scala.util.Success
import scala.util.Try
import scala.util.Failure

class DefaultV1Api(dataController: DataController)(implicit val swagger: Swagger)
    extends ScalatraServlet
    with NativeJsonSupport
    with SwaggerSupport
    with ContentEncodingSupport {

  private[this] val log = LoggerFactory.getLogger(getClass)

  protected val applicationDescription: String  = "default access API for AUV logs (v1)"
  protected val applicationName: Option[String] = Some("auvdata")
  // Sets up automatic case class to JSON output serialization
  private[this] val customInstantFormat = new CustomSerializer[Instant](format =>
    (
      { case JString(s) => Instant.parse(s) },
      { case i: Instant => JString(i.toString) }
    )
  )
  implicit protected lazy val jsonFormats: Formats = DefaultFormats + customInstantFormat

  // private[this] implicit val stringToInstant = safe { str: String => Instant.parse(str) }
  protected[this] val timeFormatter        = DateTimeFormatter.ISO_DATE_TIME
  protected[this] val compactTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd'T'HHmmssX")
  implicit protected val stringToInstant = new TypeConverter[String, Instant] {
    override def apply(s: String): Option[Instant] =
      Try(Instant.from(compactTimeFormatter.parse(s))) match {
        case Success(i) => Some(i)
        case Failure(_) => Try(Instant.from(timeFormatter.parse(s))).toOption
      }
  }

  before() {
    contentType = formats("json")
    response.headers.set("Access-Control-Allow-Origin", "*")
  }

  /*
   * -- List Deployments. Optionally constraing by start/end dates
   *
   * Examples:
   *   http://localhost:8080/auvdata/v1/deployments
   *   http://localhost:8080/auvdata/v1/deployments?from=2010-01-03T22:11:07Z&to=2010-03-11T00:00:00Z
   *   http://localhost:8080/auvdata/v1/deployments?from=2010-03-11T00:00:00Z
   *   http://localhost:8080/auvdata/v1/deployments?to=2010-03-11T00:00:00Z
   */
  val deploymentsGET = (apiOperation[List[Deployment]]("deploymentsGet")
    summary "List AUV deployments. Optionally constraing by start/end dates"
    parameters (
      queryParam[Instant]("from").description("Starting date constraint").optional,
      queryParam[Instant]("to").description("Ending date constraint").optional
  ))

  get("/deployments", operation(deploymentsGET)) {

    val from = params.getAs[Instant]("from")
    val to   = params.getAs[Instant]("to")

    if (log.isDebugEnabled) {
      log.debug(s"/deployments => from: $from, to: $to")
    }

    dataController
      .missions(from, to)
      .toList
      .map(md => md.asDeployment.asWebDeployment)
      .sortBy(d => d.name)
  }

  /*
   * -- Fetch a file from a deployment
   *
   * Examples:
   *   http://localhost:8080/auvdata/v1/files/download/2010.027.04/gps.log
   */
  val filesDownloadGET = (apiOperation[Unit]("filesDownloadGET")
    summary "Retrieve a raw file for a deployment"
    parameters (
      pathParam[String]("deployment").description("The deployment of interest"),
      pathParam[String]("vehicle").description("The name of the vehicle"),
      pathParam[String]("file").description("The file to retrieve")
  ))

  get("/files/download/:deployment/:vehicle/:file", operation(filesDownloadGET)) {
    val deployment = params.getOrElse("deployment", halt(400))
    val vehicle    = params.getOrElse("vehicle", halt(400))
    val file       = params.getOrElse("file", halt(400))
    val matchingFile = dataController
      .files(deployment)
      .filter(f => f.getFileName.toString.equals(file))

    response.setContentType("application/octet-stream")

    if (matchingFile.isEmpty) NotFound(s"$file was not found for deployment $deployment")
    else Ok(matchingFile.head.toFile) // Must convert path to file so scalatra will upload it.
  }

  /*
   * --
   *
   * Examples:
   *   http://localhost:8080/auvdata/v1/files/list/2010.027.04
   */
  val filesListGET = (apiOperation[List[File]]("filesListGET")
    summary "Fetch a listing of files for a deployment"
    parameters (
      pathParam[String]("deployment").description("The deployment of interest"),
      pathParam[String]("vehicle").description("The name of the vehicle")
  ))

  get("/files/list/:deployment/:vehicle", operation(filesListGET)) {
    val deployment = params.getOrElse("deployment", halt(400))
    val vehicle    = params.getOrElse("vehicle", halt(400))
    val fileNames = dataController
      .files(deployment)
      .map(_.getFileName.toString)
      .sortBy(_.toUpperCase)
    MissionFiles(deployment, fileNames)
  }

  /*
   * -- Parse an AUV log and return it's entire contents
   *
   * Examples
   *   http://localhost:8080/auvdata/v1/files/read/2010.027.04/gps.log
   *   http://localhost:8080/auvdata/v1/files/read/2010.027.04/gps.log?data=false
   *   http://localhost:8080/auvdata/v1/files/read/2010.027.04/gps.log?data=true
   */
  val filesReadGET = (apiOperation[Unit]("filesReadGET")
    summary "Return contents of an AUV log"
    parameters (
      pathParam[String]("deployment").description("The deployment of interest"),
      pathParam[String]("vehicle").description("The name of the vehicle"),
      pathParam[String]("file").description("The log file of interest"),
      queryParam[String]("format").description("json or csv. Not implemented yet").optional,
      queryParam[Boolean]("data")
        .description("Read the data. true is default. false returns header only")
        .optional
  ))

  get("/files/read/:deployment/:vehicle/:file", operation(filesReadGET)) {

    val deployment = params.getOrElse("deployment", halt(400))
    val vehicle    = params.getOrElse("vehicle", halt(400))
    val file       = params.getOrElse("file", halt(400))
    val format     = params.getAs[String]("format")
    val data       = params.getAs[Boolean]("data").getOrElse(true)

    // TODO handle format types

    log.debug(s"Parsing ${deployment}/${file} as ${format}")
    val logFile = dataController
      .files(deployment)
      .filter(_.getFileName.toString.equalsIgnoreCase(file))

    if (logFile.isEmpty) ""
    else if (!data) dataController.header(logFile.head)
    else dataController.data(logFile.head)

  }

  val updatePOST = (apiOperation[Unit]("updatePOST")
    summary "Tells data store to update and look for new mission directories")

  post("/deployments/update", operation(updatePOST)) {
    dataController match {
      case imdc: InMemDataController => imdc.updateAsync()
      case _                         => // Do nothing
    }
  }

}
