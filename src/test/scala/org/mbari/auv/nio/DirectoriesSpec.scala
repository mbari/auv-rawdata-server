/*
 * Copyright 2017 Monterey Bay Aquarium Research Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.mbari.auv.nio

import java.io.File

import org.scalatest.{ FlatSpec, Matchers }

/**
 * @author Brian Schlining
 * @since 2017-03-27T16:58:00
 */
class DirectoriesSpec extends FlatSpec with Matchers {

  private[this] val path = {
    val url = getClass.getResource("/data/2010322/2010.322.03")
    new File(url.toURI).toPath
  }

  "Directories" should "list all files" in {
    val xs = Directories.list(path)
    xs.size should be(5)
  }

  it should "list files filtered using a predicate function" in {
    val xs = Directories.list(path, (p) => p.endsWith("gps.log"))
    xs.size() should be(1)
  }

  it should "list files filtered using a glob expression" in {
    val xs = Directories.list(path, "gps*")
    xs.size() should be(1)

    val ys = Directories.list(path, "*.log")
    ys.size() should be(4)
  }

}
