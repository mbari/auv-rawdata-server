#!/usr/bin/env bash

echo "--- Building auv-rawdata-server"

BUILD_DATE=`date -u +"%Y-%m-%dT%H:%M:%SZ"`
VCS_REF=`git tag | sort -V | tail -1`


sbt pack && \
    docker build --build-arg BUILD_DATE=$BUILD_DATE \
                 --build-arg VCS_REF=$VCS_REF \
                  -t portal.shore.mbari.org:5000/mbari/auv-rawdata-server:${VCS_REF} \
                  -t portal.shore.mbari.org:5000/mbari/auv-rawdata-server:latest . && \
    docker push portal.shore.mbari.org:5000/mbari/auv-rawdata-server

for server in "$USER@portal.shore.mbari.org"
do

echo "--- Deploying and starting auv-rawdata-server at $server as 'docker_user'"

ssh $server <<'ENDSSH'
  docker pull portal.shore.mbari.org:5000/mbari/auv-rawdata-server
  docker stop portal.shore.mbari.org:5000/mbari/auv-rawdata-server
  docker rm -f auv-rawdata-server
  docker run -d \
    --user 11472 \
    --name=auv-rawdata-server \
    -v /home/docker_user/auv_rawdata_server:/home/docker_user/auv_rawdata_server  \
    -v /M3:/mnt/M3:ro \
    -v /AUVBI:/mnt/AUVBI:ro \
    -v /AUVCTD:/mnt/AUVCTD:ro \
    -p 8080:8080 \
    -e HTTP_CONTEXT_PATH=/auvdata \
    -e HTTP_PORT=8080 \
    -e AUV_DIRECTORIES="/mnt/M3:/mnt/AUVBI:/mnt/AUVCTD" \
    -e DATA_CACHE_FILE=/home/docker_user/auv_rawdata_server/mission-cache.json \
    --restart unless-stopped \
    portal.shore.mbari.org:5000/mbari/auv-rawdata-server:latest
ENDSSH

done