addSbtPlugin("com.timushev.sbt"  % "sbt-updates"           % "0.5.0")
addSbtPlugin("com.typesafe.sbt"  % "sbt-git"               % "1.0.0")
addSbtPlugin("de.heikoseeberger" % "sbt-header"            % "5.5.0")
addSbtPlugin("com.earldouglas"   % "xsbt-web-plugin"       % "4.1.0")
addSbtPlugin("org.scalameta"     % "sbt-scalafmt"          % "2.3.4")
addSbtPlugin("org.scalastyle"    % "scalastyle-sbt-plugin" % "1.0.0")
addSbtPlugin("org.xerial.sbt"    % "sbt-pack"              % "0.12")

resolvers += Resolver.sonatypeRepo("releases")
