# auv-rawdata-server

This is a RESTful server that returns data from the log files for an AUV missions. An AUV mission is considered a directory that contains AUV binary log files. 

When started, it reads the base directories configured in _application.conf_. It walks all the sub-directories looking for any that contain _.log_ files; those with log files are assumed to be AUV missions. The name of the directory with the log is used as the _mission_ or _deployment_ name. On the first request for a _deployments_ listing, it will read all the _navigation.logs_ and extract the start and end times of the mission. On server shutdown, this information is written to a cache file and re-read on subsequent start ups. If, for some reason, you want to reload the information from a directory, just delete the matching line from the cache file (it's just text/json) and it will be re-read from the navigation.log the next time the server is started.

## Get Started

To work with the build:

```shell
git clone https://hohonuuli@bitbucket.org/mbari/auv-rawdata-server.git
cd auv-rawdata-server
sbt
```

To deploy:

```shell
build.sh
```

## API

A swagger UI is included in this distribution. When running locally just navigate to [http://localhost:8080/ui/](http://localhost:8080/ui/) to view the auv-rawdata-server's API.


## For Developers

- `sbt compile` : Build
- `sbt jetty:start` : Run a development server
- `sbt package` : Build a war file under _target/scala_2.11_
- `sbt pack` : Build a standalone distribution under _target/pack_. The server can be run by using _bin/jetty-main_. You can configure additional AUV directories by editing _conf/application.conf_.

## Deploy

1. Configure properties in `src/pack/conf/application.conf`. The most import one is to include the full paths (relative to the docker image) to the _missionlogs_ directories.
    - I like to set the directories variable to `/mnt/AUVCTD:/mnt/AUVBI` and mount them from the host using the docker command below.
2. Build server with `sbt pack`
3. Build docker image: `docker build -t auv-rawdata-server .`
4. Launch image: `docker run -d -v /Volumes/AUVBI/missionlogs:/mnt/AUVBI:ro -v /Volumes/AUVCTD/missionlogs:/mnt/AUVCTD:ro -p 8080:8080  auv-rawdata-server`
    - Note that on Macs you can only mount directies using the path /Users/<path> as those are automounted onto the VM running docker. On linux, you can mount any path you want.
    - Omit the -d flag if you want to watch the debug output

## For End-users

The interchange format for now is JSON. Here's a few examples:

### Matlab

```
j = webread('http://portal.shore.mbari.org:8080/auvdata/v1/files/read/2010.089.02/Dorado389/navigation.log')
j(1)               % Look at the first field
j(1).description   % Description of first field
j(1).data          % Data for first field
j(1).data(1)       % First data point in the first field
```

```
% For older versions of Matlab
% Install JSONlab from https://www.mathworks.com/matlabcentral/fileexchange/33381-jsonlab--a-toolbox-to-encode-decode-json-files
r = urlread('http://portal.shore.mbari.org:8080/auvdata/v1/files/read/2010.089.02/Dorado389/navigation.log')
j = loadjson(r)
j{1}                % Look at the first field
j{1}.description    % Description of first field
j{1}.data           % Data for first field
j{1}.data(1)        % First data point in first field
```

### Python

```
import requests
r = requests.get('http://portal.shore.mbari.org:8080/auvdata/v1/files/read/2010.089.02/Dorado389/navigation.log')
import json
j = json.loads(r.text)
j[0]                # Look at the first field
j[0]['description'] # Description of first field
j[0]['data']        # Data for first field
j[0]['data'][0]     # First data point in first field
```

### Ruby

```
require 'open-uri' 
r = URI('http://portal.shore.mbari.org:8080/auvdata/v1/files/read/2010.089.02/Dorado389/navigation.log').read
require 'json'
j = JSON.parse(r)
j[0]                # Look at the first field
j[0]['description'] # Description of first field
j[0]['data']        # Data for first field
j[0]['data'][0]     # First data point in first field
```

### R

```
install.packages("rjson") 
library(rjson)
j <- fromJSON(readLines('http://portal.shore.mbari.org:8080/auvdata/v1/files/read/2010.089.02/Dorado389/navigation.log'))
j[[1]]              # Look at the first field
j[[1]]$description  # Description of first field
j[[1]]$data         # Data for first field
j[[1]]$data[1]      # First data point in first field
```

### JavaScript

```
// Using JQuery
$.getJSON('http://portal.shore.mbari.org:8080/auvdata/v1/files/read/2010.089.02/Dorado389/navigation.log', function(j) {
    //j is the JSON string
});
```

### Scala ([json4s](http://json4s.org/))

```
# Read using Scala std lib
val r = scala.io.Source.fromURL("http://portal.shore.mbari.org:8080/auvdata/v1/files/read/2010.089.02/Dorado389/navigation.log").mkString

# add a json4s library: e.g. in Ammonite: load.ivy("org.json4s" %% "json4s-native" % "3.3.0")
import org.json4s._
import org.json4s.native.JsonMethods._
val j = parse(r)

# You need the following to extract values
implicit val formats = DefaultFormats

j(0)                    // Reference to first field
j(0) \ "description"    // Reference to description of first field
(j(0) \ "description" \ "units").extract[String] // Extract the units value for the first field
val data = (j(0) \ "data").extract[Array[Double]] // Extract data as an Array for first field
data(0)                 // First data point in first field

# You can alternatively map the JSON to scala case classes.
```

## API Examples

Here’s a few example URLs:

### List all deployments

<http://portal.shore.mbari.org:8080/auvdata/v1/deployments>

### List deployments constrained by dates

<http://portal.shore.mbari.org:8080/auvdata/v1/deployments?from=20030808T173317Z&to=20040108T173317Z>

<http://portal.shore.mbari.org:8080/auvdata/v1/deployments?from=20030808T173317Z>

<http://portal.shore.mbari.org:8080/auvdata/v1/deployments?to=20040108T173317Z>

### List files for a mission/deployment

<http://portal.shore.mbari.org:8080/auvdata/v1/files/list/2003.230.03/Dorado389>

### Download a file from a mission/deployment

<http://portal.shore.mbari.org:8080/auvdata/v1/files/download/2003.230.03/Dorado389/gps.log>

### Fetch log data as JSON

<http://portal.shore.mbari.org:8080/auvdata/v1/files/read/2003.230.03/Dorado389/gps.log>
<http://portal.shore.mbari.org:8080/auvdata/v1/files/read/2003.230.03/Dorado389/gps.log?data=true>

### Fetch log metadata as JSON

<http://portal.shore.mbari.org:8080/auvdata/v1/files/read/2003.230.03/Dorado389/gps.log?data=false>

## Overview

This server was originally generated by the [swagger-codegen](https://github.com/swagger-api/swagger-codegen) project. This project uses the [scalatra](http://scalatra.org/) framework. 

## Notes

JSON responses will be compressed if the _Accept-Encoding_ header is set to _gzip_, _deflate_ or some combination of those.

[Here](http://blog.sokolenko.me/2014/11/javavm-options-production.html) are suggestions for JVM options when running the server

### Deployment

Attempted the following on _portal.shore.mbari.org_:

```
docker run -d -p 80:80 -v /var/run/docker.sock:/tmp/docker.sock:ro jwilder/nginx-proxy
docker run -e VIRTUAL_HOST=portal.shore.mbari.org -v /mbari/AUVBI/missionlogs:/mnt/AUVBI:ro -v /mbari/AUVCTD/missionlogs:/mnt/AUVCTD:ro -p 8080:8080  hohonuuli/auv-rawdata-server
```

See also 
- [https://docs.docker.com/engine/admin/host_integration/](https://docs.docker.com/engine/admin/host_integration/) on running docker containers as services
- [https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/System_Administrators_Guide/sect-Managing_Services_with_systemd-Unit_Files.html](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/System_Administrators_Guide/sect-Managing_Services_with_systemd-Unit_Files.html)
- [https://github.com/jwilder/nginx-proxy](https://github.com/jwilder/nginx-proxy)
- [https://www.nginx.com/blog/deploying-nginx-nginx-plus-docker/](https://www.nginx.com/blog/deploying-nginx-nginx-plus-docker/)